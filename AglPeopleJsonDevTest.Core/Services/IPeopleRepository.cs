﻿using System;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace AglPeopleJsonDevTest
{
    public interface IPeopleRepository
	{
		Task<IEnumerable<Person>> GetPeopleAsync(bool forceRefresh = false);

		Task<Person> GetPersonAsync(string name);

        Task<bool> AddPersonAsync(Person item);

        Task<bool> UpdatePersonAsync(Person item);

        Task<bool> DeletePersonAsync(string name);
    }
}
