﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Net.Http;
using System.Text;
using System.Threading.Tasks;

using Newtonsoft.Json;
using Plugin.Connectivity;

namespace AglPeopleJsonDevTest
{
    public class PeopleRepository : IPeopleRepository
    {
        private HttpClient _client;
        private static List<Person> _people;

        private const string BaseUrl = "https://agl-developer-test.azurewebsites.net/";

        public PeopleRepository()
        {
            _client = new HttpClient();
            _client.BaseAddress = new Uri($"{BaseUrl}/");

            _people = new List<Person>();
        }

        public async Task<IEnumerable<Person>> GetPeopleAsync(bool forceRefresh = false)
        {
            if (forceRefresh && CrossConnectivity.Current.IsConnected)
            {
                var json = await _client.GetStringAsync($"/people.json");
                var people = await Task.Run(() => JsonConvert.DeserializeObject<List<Person>>(json));
                _people = people.ToList();
            }

            return _people;
        }

        public async Task<Person> GetPersonAsync(string name)
        {
            // Note: Would ordinarily be properly implemented with full API service
            if (_people == null || !_people.Any())
            {
                await GetPeopleAsync(true);
            }

            if (name != null && CrossConnectivity.Current.IsConnected)
            {
                return _people.FirstOrDefault(p => p.Name == name);
            }

            return null;
        }

        public async Task<bool> AddPersonAsync(Person person)
        {
            // Note: Would ordinarily be properly implemented with full API service
            var tcs = new TaskCompletionSource<bool>();

            await Task.Run(() =>
            {
                if (person == null)
                {
                    tcs.SetResult(false);
                }

                _people.Add(person);

                tcs.SetResult(true);
            });

            return await tcs.Task;
        }

        public async Task<bool> UpdatePersonAsync(Person person)
        {
            // Note: Would ordinarily be properly implemented with full API service
            var tcs = new TaskCompletionSource<bool>();

            await Task.Run(() =>
            {
                if (person == null || person.Name == null)
                {
                    tcs.SetResult(false);
                }

                var existingPerson = _people.FirstOrDefault(p => p.Name == person.Name);
                if (existingPerson == null)
                {
                    tcs.SetResult(false);
                }
                _people[_people.IndexOf(existingPerson)] = person;
                tcs.SetResult(true);
            });

            return await tcs.Task;
        }

        public async Task<bool> DeletePersonAsync(string name)
        {
            // Note: Would ordinarily be properly implemented with full API service
            var tcs = new TaskCompletionSource<bool>();

            await Task.Run(() =>
            {
                if (string.IsNullOrEmpty(name))
                {
                    tcs.SetResult(false);
                }

                var existingPerson = _people.FirstOrDefault(p => p.Name == name);
                if (existingPerson == null)
                {
                    tcs.SetResult(false);
                }
                _people.RemoveAt(_people.IndexOf(existingPerson));
                tcs.SetResult(true);
            });

            return await tcs.Task;
        }
    }
}
