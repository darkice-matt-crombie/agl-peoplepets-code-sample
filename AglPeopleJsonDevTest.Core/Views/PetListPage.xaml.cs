﻿using System;
using System.Collections.Generic;
using System.Threading.Tasks;

using Xamarin.Forms;

namespace AglPeopleJsonDevTest
{
    public partial class PetListPage : ContentPage
    {
        PetListViewModel viewModel;

        public PetListPage()
        {
            InitializeComponent();

            BindingContext = viewModel = new PetListViewModel();
        }

        private async void OnItemSelected(object sender, SelectedItemChangedEventArgs args)
        {
            var person = args.SelectedItem as Person;
            if (person == null)
            {
                return;
            }

            MessagingCenter.Send(new MessagingCenterAlert
            {
                Title = "This is just a sample app",
                Message = "Would ordinarily wire this up and allow adding of new items.",
                Cancel = "OK"
            }, "message");

            // Manually deselect item
            PetListView.SelectedItem = null;
        }

        private void OnFilterToggle(object sender, EventArgs e)
		{
            viewModel.FilterCatsOnly = !viewModel.FilterCatsOnly;
                     
			((ToolbarItem)sender).Text = viewModel.FilterCatsOnly ? "Show all pets" : "Show cats only";

            OnRefreshing(PetListView, null);
        }

        private async void OnRefreshing(object sender, EventArgs e)
        {
            ListView lv = (ListView)sender;

            await viewModel.ExecuteLoadItemsCommand();

            lv.IsRefreshing = false;
        }

        protected override void OnAppearing()
        {
            base.OnAppearing();

            if (viewModel.ListItems.Count == 0)
            {
                viewModel.LoadItemsCommand.Execute(null);
            }
        }
    }
}
