﻿using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Diagnostics;
using System.Linq;
using System.Threading.Tasks;

using Xamarin.Forms;

namespace AglPeopleJsonDevTest
{
    public class PetGroup : ObservableCollection<Pet>
    {
        public string Key { get; private set; }
        public PetGroup(string key, IEnumerable<Pet> items)
        {
            Key = key;
            AddItems(items);
        }

        public void AddItems(IEnumerable<Pet> items)
        {
            foreach (var item in items)
            {
                Items.Add(item);
            }
        }
    }

    public class PetListViewModel : BaseViewModel
    {
        public ObservableCollection<PetGroup> ListItems { get; set; }

        public Command LoadItemsCommand { get; set; }

        private bool _filterCatsOnly;
        public bool FilterCatsOnly
        {
            get { return _filterCatsOnly; }
            set
            {
                if (value != _filterCatsOnly)
                {
                    _filterCatsOnly = value;
                    OnPropertyChanged();
                }
            }
        }

        public PetListViewModel()
        {
            Title = "Pets";
            ListItems = new ObservableCollection<PetGroup>();
            LoadItemsCommand = new Command(async () => await ExecuteLoadItemsCommand());
        }

        public async Task ExecuteLoadItemsCommand()
        {
            if (IsBusy)
            {
                return;
            }

            IsBusy = true;

            try
            {
                ListItems.Clear();

                // /Get/Refresh data from service
                var people = await DataStore.GetPeopleAsync(true);

                // Filter by cats only if requestedß
                Func<Pet, bool> petTypeFilter = f => true;
                if (FilterCatsOnly)
                {
                    petTypeFilter = f => f.Type == "Cat";
                }

                // Transform/massage the data into the structure we want to display on screen
                var groupedPets = people
                    .Where(x => x.Pets != null && x.Pets.Any())
                    .GroupBy(p => p.Gender, c => c)
                    .Select(g => 
                            new PetGroup(g.Key, 
                                         g.SelectMany(p => p.Pets.Where(petTypeFilter))
                                         .OrderBy(o => o.Name)))
                    .OrderByDescending(p => p.Key)
                    .ToList();

                foreach (var item in groupedPets)
                {
                    ListItems.Add(item);
                }
            }
            catch (Exception ex)
            {
                Debug.WriteLine(ex);
                MessagingCenter.Send(new MessagingCenterAlert
                {
                    Title = "Error",
                    Message = "Unable to load people.",
                    Cancel = "OK"
                }, "message");
            }
            finally
            {
                IsBusy = false;
            }
        }
    }
}
