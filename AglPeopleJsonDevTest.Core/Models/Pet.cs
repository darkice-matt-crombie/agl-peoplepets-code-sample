﻿using System;

using Newtonsoft.Json;

namespace AglPeopleJsonDevTest
{
	public class Pet : ObservableObject
	{
		private string _name = string.Empty;

		[JsonProperty(PropertyName = "name")]
		public string Name
		{
			get { return _name; }
			set { SetProperty(ref _name, value); }
		}

		private string _type = string.Empty;

		[JsonProperty(PropertyName = "type")]
		public string Type
		{
			get { return _type; }
			set { SetProperty(ref _type, value); }
		}

		private string _ownerName = string.Empty;

		[JsonIgnore]
		public string OwnerName
		{
			get { return _ownerName; }
			set { SetProperty(ref _ownerName, value); }
		}

		[JsonIgnore]
        public string TypeImage => Type + ".png";
	}
}
