﻿using System;
using System.Collections.Generic;
using Newtonsoft.Json;

namespace AglPeopleJsonDevTest
{
    public class Person : ObservableObject
    {
        private string _name = string.Empty;

        [JsonProperty(PropertyName = "name")]
        public string Name
        {
            get { return _name; }
            set { SetProperty(ref _name, value); }
		}

		private string _gender = string.Empty;

		[JsonProperty(PropertyName = "gender")]
		public string Gender
		{
			get { return _gender; }
			set { SetProperty(ref _gender, value); }
		}

		private int _age;

		[JsonProperty(PropertyName = "age")]
		public int Age
		{
			get { return _age; }
			set { SetProperty(ref _age, value); }
		}

		private List<Pet> _pets;

		[JsonProperty(PropertyName = "pets")]
		public List<Pet> Pets
		{
			get { return _pets; }
			set { SetProperty(ref _pets, value); }
		}
    }
}
