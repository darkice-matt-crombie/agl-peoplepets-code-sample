﻿using System.Collections.Generic;
using System.Linq;
using Xamarin.Forms;

namespace AglPeopleJsonDevTest
{
    public partial class App : Application
    {
        public App()
        {
            InitializeComponent();

            // Setup IoC
            DependencyService.Register<PeopleRepository>();

			SetMainPage();
        }

        public static void SetMainPage()
        {

            GoToMainPage();
        }

        public static void GoToMainPage()
        {
            Current.MainPage = new TabbedPage
            {
                Children = {
                    new NavigationPage(new PetListPage())
                    {
                        Title = "People's Pets",
                        Icon = Device.OnPlatform("tab_feed.png", null, null)
                    },
                    new NavigationPage(new AboutPage())
                    {
                        Title = "About",
                        Icon = Device.OnPlatform("tab_about.png", null, null)
                    },
                }
            };
        }
    }
}
