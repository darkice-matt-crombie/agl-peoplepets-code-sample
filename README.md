# README #

This is a sample for AGL based on the requirements at https://agl-developer-test.azurewebsites.net/

**Note: I read the instructions again after completing the exercise and realised there was a requirement to filter only cats as pets instead of all pets. To correct this I added a toggle to filter by cats only or all pets.**

## Overview ##

Matt Crombie's AGL People + Pets Code Sample

* Only iOS and Android have been done for this sample, not UWP
* It uses Xamarin.Forms as the main framework
* It uses an Mvvm pattern to make components more easily testable
* CRUD service calls have been mocked out for the purposes of testing
* JSON is handled by Json.NET.
* LINQ is being used to massage the data into desired output, as per note encouraging this
* Basic unit tests have been written using NUnit
* The unit tests emulate as much of the app as possible using Xamarin.Forms.Mocks
* Basic UI tests have been written using Xamarin UI Test for both iOS and Android

As this is just a sample so not everything is fully implemented and tested.

Time to complete, approximately 2 hours

## Screenshots ##

### iOS ###
![Scheme](Screenshots/ios_home.png)
![Scheme](Screenshots/ios_home2.png)
![Scheme](Screenshots/ios_about.png)

### Android ###
![Scheme](Screenshots/droid_home.png)
![Scheme](Screenshots/droid_home2.png)
![Scheme](Screenshots/droid_about.png)

### Unit and UI Tests ###
![Scheme](Screenshots/tests.png)

## Running UI tests ##

Tests would normally be a lot more intelligent but for the purposes of this sample it is just getting the minimum functionality. 

To run the tests the app must be installed on the device or simulator as a UITest build.
For Android it will connect to the only device connected. If multiple devices are running id must be specified.