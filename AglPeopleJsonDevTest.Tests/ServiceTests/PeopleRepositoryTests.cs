﻿using NUnit.Framework;
using System;
using System.Linq;
using System.Threading.Tasks;
using Xamarin.Forms;
using System.Collections.Generic;

namespace AglPeopleJsonDevTest.Tests
{
	[TestFixture()]
	public class PeopleRepositoryTests : BaseTest
	{
		// Note: Would usually mock out some data and would split out unit and integration tests, but this is just a sample

		public IPeopleRepository DataStore => DependencyService.Get<IPeopleRepository>();

		[Test()]
		public void PetListViewModel_LoadData_IsValid()
		{
            // Arrange / Act
            var people = DataStore.GetPeopleAsync(true).Result.ToList();

			// Assert
			Assert.IsNotNull(people);
			Assert.AreEqual(6, people.Count);
		}

		[Test()]
		public void PetListViewModel_LoadData_CacheIsValid()
		{
			// Arrange / Act
			var people = DataStore.GetPeopleAsync(true).Result.ToList();

			// Assert
			Assert.IsNotNull(people);
			Assert.AreEqual(6, people.Count);

			// Act
			var deleteResult = DataStore.DeletePersonAsync(people.FirstOrDefault().Name).Result;
			var cachedPeople = DataStore.GetPeopleAsync(false).Result.ToList();

			// Assert
			Assert.IsNotNull(cachedPeople);
			Assert.AreEqual(5, cachedPeople.Count);
		}

        [Test()]
        public void PetListViewModel_Add_Succeeds()
        {
            // Arrange / Act
            var person = new Person
            {
                Name = "Test",
                Age = 30,
                Pets = new List<Pet>
                {
                    new Pet{ Name = "Rover", Type = "Dog"},
                },
            };

            // Act
            var addResult = DataStore.AddPersonAsync(person).Result;

            // Assert
            Assert.IsTrue(addResult);
		}

		[Test()]
		public void PetListViewModel_Update_Succeeds()
		{
			// Arrange / Act
			var people = DataStore.GetPeopleAsync(true).Result.ToList();
            var person1 = people.FirstOrDefault();
            person1.Name = "Matt";

			// Act
			var updateResult = DataStore.UpdatePersonAsync(person1).Result;

			// Assert
			Assert.IsTrue(updateResult);

			// Act
			var updatedPeople = DataStore.GetPeopleAsync(false).Result.ToList();

            // Assert
            Assert.AreEqual("Matt", updatedPeople.FirstOrDefault().Name);
		}

		[Test()]
		public void PetListViewModel_Delete_Succeeds()
		{
			// Arrange / Act
			var people = DataStore.GetPeopleAsync(true).Result.ToList();

			// Assert
			Assert.IsNotNull(people);
			Assert.AreEqual(6, people.Count);

			// Act
			var deleteResult = DataStore.DeletePersonAsync(people.FirstOrDefault().Name).Result;
			var updatedPeople = DataStore.GetPeopleAsync(false).Result.ToList();

			// Assert
			Assert.IsNotNull(updatedPeople);
			Assert.AreEqual(5, updatedPeople.Count);
		}
	}
}
