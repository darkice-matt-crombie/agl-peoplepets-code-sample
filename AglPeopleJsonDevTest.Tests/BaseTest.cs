﻿using System;
using NUnit.Framework;
using Xamarin.Forms;

namespace AglPeopleJsonDevTest.Tests
{
    public abstract class BaseTest
    {

        [SetUp]
        public void Init()
		{
			// Use mock Xamarin.Forms so we can test more throughly against code that is dependent against forms functionality
			Xamarin.Forms.Mocks.MockForms.Init();

            // Setup IoC
            DependencyService.Register<PeopleRepository>();
        }

        [TearDown]
        public void CleanUp()
        {
            
        }
    }
}
