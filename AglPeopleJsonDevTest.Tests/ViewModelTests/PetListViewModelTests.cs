﻿﻿using NUnit.Framework;
using System;
using System.Linq;
using System.Threading.Tasks;

namespace AglPeopleJsonDevTest.Tests
{
    [TestFixture()]
    public class PetListViewModelTests : BaseTest
    {
        // Note: Would usually mock out some data and would split out unit and integration tests, but this is just a sample

        [Test()]
        public void PetListViewModel_LoadData_ReturnsNewData()
        {
			// Arrange
			var petsListViewModel = new PetListViewModel();

            // Act
            petsListViewModel.ExecuteLoadItemsCommand().Wait();

            // Assert
            Assert.IsNotNull(petsListViewModel.ListItems);
            Assert.Greater(petsListViewModel.ListItems.Count(), 0);
		}

		[Test()]
		public void PetListViewModel_LoadData_GroupingOrderCorrect()
		{
			// Arrange
			var petsListViewModel = new PetListViewModel();

			// Act
			petsListViewModel.ExecuteLoadItemsCommand().Wait();

            // Assert
            var expectedGroupOrder = new[]
            {
                "Male",
                "Female",
			};
			var actualGroupOrder = petsListViewModel.ListItems.Select(p => p.Key);

			CollectionAssert.AreEqual(expectedGroupOrder, actualGroupOrder);
		}

		[Test()]
		public void PetListViewModel_LoadData_PetOrderCorrect()
		{
			// Arrange
			var petsListViewModel = new PetListViewModel();

			// Act
			petsListViewModel.ExecuteLoadItemsCommand().Wait();

            // Assert
            var expectedMalePets = new[]
            {
				"Fido",
				"Garfield",
				"Jim",
				"Max",
				"Sam",
				"Tom",
            };
            var actualMalePets = petsListViewModel.ListItems[0].Select(p => p.Name);

			CollectionAssert.AreEqual(expectedMalePets, actualMalePets);

			var expectedFemalePets = new[]
			{
				"Garfield",
				"Nemo",
				"Simba",
				"Tabby",
			};
			var actualFemalePets = petsListViewModel.ListItems[1].Select(p => p.Name);

			CollectionAssert.AreEqual(expectedFemalePets, actualFemalePets);
		}

		[Test()]
		public void PetListViewModel_LoadData_FilterCatsOnly()
		{
            // Arrange
            var petsListViewModel = new PetListViewModel()
            {
                FilterCatsOnly = true,
            };

			// Act
			petsListViewModel.ExecuteLoadItemsCommand().Wait();

			// Assert
			var expectedMalePets = new[]
			{
				"Garfield",
				"Jim",
				"Max",
				"Tom",
			};
			var actualMalePets = petsListViewModel.ListItems[0].Select(p => p.Name);

			CollectionAssert.AreEqual(expectedMalePets, actualMalePets);

			var expectedFemalePets = new[]
			{
				"Garfield",
				"Simba",
				"Tabby",
			};
			var actualFemalePets = petsListViewModel.ListItems[1].Select(p => p.Name);

			CollectionAssert.AreEqual(expectedFemalePets, actualFemalePets);
		}
    }
}
