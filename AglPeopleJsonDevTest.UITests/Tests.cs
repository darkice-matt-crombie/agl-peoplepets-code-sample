﻿using System;
using System.IO;
using System.Linq;
using NUnit.Framework;
using Xamarin.UITest;
using Xamarin.UITest.Queries;

namespace AglPeopleJsonDevTest.UITests
{
    [TestFixture(Platform.Android)]
    [TestFixture(Platform.iOS)]
    public class Tests
    {
        IApp app;
        Platform platform;

        public Tests(Platform platform)
        {
            this.platform = platform;
        }

        [SetUp]
        public void BeforeEachTest()
        {
            app = AppInitializer.StartApp(platform);
        }

        [Test]
        public void AppLaunches()
        {
            app.Screenshot("HomeScreen");
		}

		[Test]
		public void DataLoads()
		{
            // Wait for data to load by looking for first and last element
			app.WaitForElement(_=>_.Marked("Male"));
			app.ScrollDown();
			app.WaitForElement(_ => _.Marked("Tabby"));


            // Note: Would normally have a lot more in here checking UI elements and going through workflows but this is just a sample
		}
    }
}
