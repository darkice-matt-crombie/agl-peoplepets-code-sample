﻿using System;
using Xamarin.UITest.Queries;
using Xamarin.UITest.Configuration;
using System.Diagnostics;
using Xamarin.UITest;
using System.Collections.Generic;
using System.Linq;
using Xamarin.UITest.iOS;
using System.Globalization;
using System.Text.RegularExpressions;

namespace AglPeopleJsonDevTest.UITests.Utilities
{
	public static class IosUtilities
    {
		#region Simulator Functionality

		public static iOSAppConfigurator SetDeviceByName(this iOSAppConfigurator configurator, string simulatorName)
		{
			var deviceId = GetDeviceID(simulatorName);
			return configurator.DeviceIdentifier(deviceId);
		}

		public static void ResetSimulatorId(string deviceId)
		{
			// Shutdown the simulator if it's open or erase won't work
			var process = Process.Start("xcrun", "simctl shutdown " + deviceId);
			process.WaitForExit();

			// Erase the simulator data
			process = Process.Start("xcrun", "simctl erase " + deviceId);
			process.WaitForExit();
		}

		public static void ResetSimulator(string simulatorName)
		{
			var deviceId = GetDeviceID(simulatorName);

			ResetSimulatorId(deviceId);
		}

		public static void GetDeviceID(string preferredSimulatorName, out string simulatorName, out string deviceId)
		{

			if (!TestEnvironment.Platform.Equals(TestPlatform.Local))
			{
				simulatorName = null;
				deviceId = null;
			}

			// See below for the InstrumentsRunner class.
			IEnumerable<Simulator> simulators = new InstrumentsRunner().GetListOfSimulators();

			var simulator = simulators
				.Where(x => x.Name.Equals(preferredSimulatorName) || (x.Name.StartsWith(preferredSimulatorName + " (") && !x.IsAppleWatch))
				.OrderByDescending(x => x.Name)
				.FirstOrDefault();

			if (simulator == null)
			{
				simulatorName = null;
				deviceId = null;

				throw new ArgumentException("Could not find a device identifier for '" + simulatorName + "'.", "simulatorName");
			}
			else
			{
				simulatorName = simulator.Name;
				deviceId = simulator.GUID;
			}

		}

		public static string GetDeviceID(string simulatorName)
		{
			if (!TestEnvironment.Platform.Equals(TestPlatform.Local))
			{
				return string.Empty;
			}

			// See below for the InstrumentsRunner class.
			IEnumerable<Simulator> simulators = new InstrumentsRunner().GetListOfSimulators();

			var simulator = (from sim in simulators
							 where sim.Name.Equals(simulatorName)
							 select sim).FirstOrDefault();

			if (simulator == null)
			{
				throw new ArgumentException("Could not find a device identifier for '" + simulatorName + "'.", "simulatorName");
			}
			else
			{
				return simulator.GUID;
			}
		}

		public static string GetDeviceName(string deviceId)
		{
			if (!TestEnvironment.Platform.Equals(TestPlatform.Local))
			{
				return string.Empty;
			}

			// See below for the InstrumentsRunner class.
			var devices = new InstrumentsRunner().GetListOfSimulators();

			var device = devices.FirstOrDefault(x => x.GUID.Replace("-", "").ToLower() == deviceId.Replace("-", "").ToLower());

			if (device == null)
			{
				throw new ArgumentException("Could not find a device name for '" + deviceId + "'.");
			}
			else
			{
				return device.Name;
			}
		}

		class InstrumentsRunner
		{
			static string[] GetInstrumentsOutput()
			{
				const string cmd = "/usr/bin/xcrun";

				var startInfo = new ProcessStartInfo
				{
					FileName = cmd,
					Arguments = "instruments -s devices",
					RedirectStandardOutput = true,
					UseShellExecute = false
				};

				Process proc = new Process();
				proc.StartInfo = startInfo;
				proc.Start();
				var result = proc.StandardOutput.ReadToEnd();
				proc.WaitForExit();

				var lines = result.Split('\n');
				return lines;
			}

			public Simulator[] GetListOfSimulators()
			{
				var simulators = new List<Simulator>();
				var lines = GetInstrumentsOutput();

				foreach (var line in lines)
				{
					var sim = new Simulator(line);
					if (sim.IsValid())
					{
						simulators.Add(sim);
					}
				}

				return simulators.ToArray();
			}
		}

		class Simulator
		{
			public Simulator(string line)
			{
				ParseLine(line);
			}

			public string Line { get; private set; }

			public string GUID { get; private set; }

			public string Name { get; private set; }

			public bool IsAppleWatch { get; private set; }

			public bool IsValid()
			{
				return !string.IsNullOrWhiteSpace(GUID) && !(string.IsNullOrWhiteSpace(Name));
			}

			public override string ToString()
			{
				return Line;
			}

			void ParseLine(string line)
			{

				GUID = string.Empty;
				Name = string.Empty;
				Line = string.Empty;

				if (string.IsNullOrWhiteSpace(line))
				{
					return;
				}
				Line = line.Trim();
				var idx1 = line.IndexOf(" [", StringComparison.Ordinal);
				if (idx1 < 1)
				{
					return;
				}
				var idx2 = line.LastIndexOf("]", StringComparison.Ordinal);
				if (idx2 < 1)
				{
					return;
				}

				var idStart = idx1 + 2;
				var idEnd = idx2 - (idx1 + 2);
				Name = Line.Substring(0, idx1).Trim();
				GUID = Line.Substring(idStart, idEnd).Trim();
				IsAppleWatch = line.Contains("Apple Watch");
			}
		}

		#endregion
	}
}
