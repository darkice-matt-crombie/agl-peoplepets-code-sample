﻿using System;
using System.IO;
using System.Linq;
using AglPeopleJsonDevTest.UITests.Utilities;
using Xamarin.UITest;
using Xamarin.UITest.Queries;

namespace AglPeopleJsonDevTest.UITests
{
    public class AppInitializer
    {
        public static IApp StartApp(Platform platform)
        {
            // NOTE: This would normally be a lot more intelligent but for the purposes of this sample it is just
            // getting minimum functional. 
            // 
            // To run the tests the app must be installed on the device or simulator as a UITest build.
            // For Android it will connect to the only device connected. If multiple devices are running id must be specified.

            if (platform == Platform.Android)
            {
                return ConfigureApp
                    .Android
                    .InstalledApp("com.darkiceinteractive.aglpeoplejsondevtest")
                    .StartApp();
            }

            // Try running up tests on preferred simulator
            var preferredDevice = "iPhone 7 Plus";
            string deviceId = null;
            string simulator = null;

            IosUtilities.GetDeviceID(preferredDevice, out simulator, out deviceId);

            return ConfigureApp
				.iOS
                .InstalledApp("com.darkiceinteractive.aglpeoplejsondevtest")
                .DeviceIdentifier(deviceId)
                .StartApp();
        }
    }
}
